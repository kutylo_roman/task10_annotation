package com.kutylo.model;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Model {


    public String testAnnotation() {
        StringBuilder sb = new StringBuilder();

        MyClass myClass = new MyClass();
        Class MyClassClass = MyClass.class;
        Field[] fields = MyClassClass.getDeclaredFields();
        for (Field f : fields) {
            if (f.isAnnotationPresent(Info.class)) {
                Info info = f.getAnnotation(Info.class);
                sb.append("Name: " + info.name() +
                        "\nAge: " + info.age());
            }
        }
        return sb.toString();
    }

    //task4,6
    @Deprecated
    public String invokeMethods() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        StringBuilder sb = new StringBuilder();
        MyClass myClass = new MyClass();
        Class myClassClass = myClass.getClass();
        Method method = myClassClass.getDeclaredMethod("testMethod");
        sb.append(method.invoke(myClass));
        method = myClassClass.getDeclaredMethod("testMethod", new Class[]{int[].class});
        method.setAccessible(true);
        int[] intArray = new int[]{5, 6, 4, 3, 2, 5};
        sb.append(method.invoke(myClass, intArray) + "\n");
        method = myClassClass.getDeclaredMethod("testMethod", new Class[]{String.class, int[].class});
        method.setAccessible(true);
        sb.append(method.invoke(myClass, new String("int array: "), new int[]{5, 3, 5, 4, 37, 3, 5}));
        method = myClassClass.getDeclaredMethod("testMethod", new Class[]{String.class, String[].class});
        method.setAccessible(true);
        sb.append(method.invoke(myClass, new String("\nString array: "), new String[]{"one", "two", "three"}));
        return sb.toString();
    }

    //task5
    public String setUnknownValue() throws NoSuchFieldException, IllegalAccessException {
        MyClass myClass = new MyClass();
        Class MyClassClass = MyClass.class;
        Field[] fields = MyClassClass.getDeclaredFields();
        Field field = MyClassClass.getDeclaredField("customField");
        field.set(myClass, "New value");
        return field.getName() + ": " + field.get(myClass);
    }

    //task7
    public <T> String showInfo(T obj) {
        MyClass myClass = new MyClass();
        return myClass.showInfo(obj);
    }


}
