package com.kutylo.model;

@FunctionalInterface
public interface Printable {
    void print();
}
