package com.kutylo.view;

import com.kutylo.controller.Controller;
import com.kutylo.model.Printable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;


public class ConsoleView {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input;
    private static Logger logger = LogManager.getLogger(ConsoleView.class);



    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Test Annotation");
        menu.put("2", "2 - Get annotation info of object i");
        menu.put("Q", "Q - exit");
    }

    public ConsoleView() {

        controller = new Controller();
        input = new Scanner(System.in);
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::testAnnotation);
        methodsMenu.put("2", this::testAnnotationFromClass);
        methodsMenu.put("3", this::invokeMethods);
        methodsMenu.put("4", this::setValueOfUnknowingType);
        methodsMenu.put("5", this::getObjectInfo);


    }

    private void testAnnotation(){
        logger.info(controller.testAnnotation());
    }

    private void testAnnotationFromClass(){
        logger.info(controller.testAnnotationFromClass());
    }

    private void invokeMethods(){
        logger.info(controller.invokeMethods());
    }

    private void setValueOfUnknowingType(){
        logger.info(controller.setValueOfUnknowingType());
    }
    private void getObjectInfo(){
        logger.info(controller.showInfo());
    }

    //-------------------------------------------------------------------------
    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }
}

