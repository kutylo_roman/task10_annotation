package com.kutylo.controller;

import com.kutylo.model.Model;
import com.kutylo.model.MyClass;

import java.lang.reflect.InvocationTargetException;

public class Controller {
    Model model = new Model();

    public String testAnnotation() {
        return model.testAnnotation();
    }

    public String testAnnotationFromClass() {
        String str;
        try {
            str = new MyClass().getName();
        } catch (NoSuchFieldException e) {
            return "NoSuchFieldException";
        }
        return str;
    }

    public String invokeMethods() {
        String str = null;
        try {
            str = model.invokeMethods();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String setValueOfUnknowingType() {
        String string = new String();

        try {
            string = model.setUnknownValue();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return string;
    }

    public String showInfo(){
       return model.showInfo(new Integer(67));
    }
}
